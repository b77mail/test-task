<?php

/*
 *  Brackets correct order checking function.
 *  We use stack to determine if order of brackets is corretct.
 *  
 * @param string $s string to check for correct brackets order
 * @return true|false returns true if brackets order is correct
                      returns false in case of incorrect order or string without brackets
*/

function checkBrackets($s) {
    $stack = array();
    $stringLength = strlen($s);
    
    // Check if input string is empty or without any brackets.
    if ( !$stringLength ||
        ( stripos($s, '(') === false && stripos($s, '[') === false && stripos($s, '{') === false )
    ) { return false; }
    
    for ($i = 0; $i < $stringLength; $i++) {
        // If current symbol is opening bracket then save it to stack.
        if ( $s[$i] === '(' || 
             $s[$i] === '[' || 
             $s[$i] === '{'
        ) {
            array_push($stack, $s[$i]);
        } else if ( $s[$i] === ')' && end($stack) === '(') {
            // If this is closing bracket and corresponding opening bracket
            // is found in stack then we delete last element in stack.
            array_pop($stack);
        } else if ( $s[$i] === ']' && end($stack) === '[') {
            array_pop($stack);
        } else if ( $s[$i] === '}' && end($stack) === '{' ) {
            array_pop($stack);
        }
    }
    
    // If stack is empty then order of brackets placement is correct.
    return (sizeof($stack) == 0) ? true : false;
}