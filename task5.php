<?php

/*
 *  Detects intersection between 2 lines function.
 *  
 * @param int $k angle coefficient
 * @param int $b y-intercept constant
 * @param int $x1 line1 x coordinate
 * @param int $y1 line1 y coordinate
 * @param int $x2 line2 x coordinate
 * @param int $y2 line2 y coordinate
 * @return true|false returns true if there is intersection between 2 lines
                      returns false otherwise
*/

function isIntersect($k, $b, $x1, $y1, $x2, $y2) {
    return ($k * $x1 + $b - $y1) * ($k * $x2 + $b - $y2) <= 0;
}