<?php

/*
 *  Reverse string function.
 *  We use stringLength / 2 iterations inside loop to revert string. 
 *  
 * @param string $s string to reverse
 * @return string reversed $s
*/

function myStrRev($s) {
    $tmpStr = '';
    $stringLength = strlen($s);
    $halfLength = (int)($stringLength / 2);
    
    // Check if input string is less than 2 symbols.
    if ( $stringLength < 2) { return $s; }
    
    for ($i = 0; $i < $halfLength; $i++) {
        $tmpStr = $s[$i];
        $s[$i] = $s[$stringLength - 1 - $i];
        $s[$stringLength - 1 - $i] = $tmpStr;
    }
    
    return $s;
}