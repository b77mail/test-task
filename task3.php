<?php

/*
 *  Reverse array`s keys function.
 *  We use minimum number of loops (0).
 *  
 * @param array $a array to reverse keys in
 * @return array $a with reversed keys
*/

function arrayReverseKeys($a) {
    // Check if input array has less than 2 elements.
    return ( sizeof($a) < 2) ? $a : array_combine(array_reverse(array_keys($a)), array_values($a));
}