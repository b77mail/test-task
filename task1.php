<?php

/*
 *  Binary search function in sorted array.
 *  We use binary search algorithm as the fastest way to get element in sorted array.
 * 
 * @param array $a sorted array
 * @param int $b search value
 * @return true|false returns true if search value is present in array 
                      returns false in case of any errors and if $b is missing in array
*/

function searchInArray($a, $b) {
    $arrayLength = sizeof($a);
    // Check if array is empty or $b is outside array`s range.
    if ( !$arrayLength ||
        $b < $a[0] ||
        $b > $a[$arrayLength - 1]
    ) { return false; }
    
    $leftPosition = 0;
    $rightPosition = $arrayLength - 1;

    while ( $leftPosition < $rightPosition ) {
        // Array is devided in half.
        $middlePosition = (int)floor($leftPosition + ($rightPosition - $leftPosition) / 2);

        if ( $b <= $a[$middlePosition] )  {
            $rightPosition = $middlePosition;
        } else {
            $leftPosition = $middlePosition + 1;
        }
    }

    // Return true in case if search value is found.
    return ( $a[$rightPosition] === $b ) ? true : false;
}